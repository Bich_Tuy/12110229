﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiTi.Models
{
    public class Comment
    {
        public int CommentID { set; get; }
        public String Content { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdate { set; get; }
        public String Author { set; get; }

        public virtual Post Post { set; get; }
        public int PostID { set; get; }
    }
}