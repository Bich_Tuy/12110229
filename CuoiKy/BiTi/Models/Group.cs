﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiTi.Models
{
    public class Group
    {
        public int GroupID { set; get; }
        public String GroupName { set; get; }
        public int NumberOfMember { set; get; }
        public DateTime DateCreated { set; get; }

        public virtual ICollection<UserProfile> UserProfiles { set; get; }
        
    }
}