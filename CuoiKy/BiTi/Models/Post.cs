﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BiTi.Models
{
    public class Post
    {
        public int PostID { set; get; }
        public String Title { set; get; }
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}