namespace BiTi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        GroupID = c.Int(nullable: false, identity: true),
                        GroupName = c.String(),
                        NumberOfMember = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.GroupID);
            
            CreateTable(
                "dbo.Group_User",
                c => new
                    {
                        GroupID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupID, t.UserID })
                .ForeignKey("dbo.Groups", t => t.GroupID, cascadeDelete: true)
                .ForeignKey("dbo.UserProfile", t => t.UserID, cascadeDelete: true)
                .Index(t => t.GroupID)
                .Index(t => t.UserID);
            
            AddColumn("dbo.Comments", "Content", c => c.String());
            DropColumn("dbo.Comments", "Body");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comments", "Body", c => c.String());
            DropIndex("dbo.Group_User", new[] { "UserID" });
            DropIndex("dbo.Group_User", new[] { "GroupID" });
            DropForeignKey("dbo.Group_User", "UserID", "dbo.UserProfile");
            DropForeignKey("dbo.Group_User", "GroupID", "dbo.Groups");
            DropColumn("dbo.Comments", "Content");
            DropTable("dbo.Group_User");
            DropTable("dbo.Groups");
        }
    }
}
