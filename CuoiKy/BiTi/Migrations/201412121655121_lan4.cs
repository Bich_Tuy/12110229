namespace BiTi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.TagPosts", "Post_PostID", "dbo.Posts");
            DropIndex("dbo.TagPosts", new[] { "Tag_TagID" });
            DropIndex("dbo.TagPosts", new[] { "Post_PostID" });
            CreateTable(
                "dbo.Post_Tag",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
            DropTable("dbo.TagPosts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TagPosts",
                c => new
                    {
                        Tag_TagID = c.Int(nullable: false),
                        Post_PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tag_TagID, t.Post_PostID });
            
            DropIndex("dbo.Post_Tag", new[] { "TagID" });
            DropIndex("dbo.Post_Tag", new[] { "PostID" });
            DropForeignKey("dbo.Post_Tag", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Post_Tag", "PostID", "dbo.Posts");
            DropTable("dbo.Post_Tag");
            CreateIndex("dbo.TagPosts", "Post_PostID");
            CreateIndex("dbo.TagPosts", "Tag_TagID");
            AddForeignKey("dbo.TagPosts", "Post_PostID", "dbo.Posts", "PostID", cascadeDelete: true);
            AddForeignKey("dbo.TagPosts", "Tag_TagID", "dbo.Tags", "TagID", cascadeDelete: true);
        }
    }
}
