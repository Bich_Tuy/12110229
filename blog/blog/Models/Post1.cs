﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blog.Models
{
    public class Post1
    {
        public int ID { set; get; }
        public string Title { set; get; }
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<comment> comments { set; get; }
        public int AccountID { set; get; }
    }
}