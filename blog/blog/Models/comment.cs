﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace blog.Models
{
    public class comment
    {
        public int ID { set; get; }
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        public String Author { set; get; }

        public int PostID { set; get; }
        public virtual Post1 Post { set; get; }
    }
}