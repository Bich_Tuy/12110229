namespace blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Post", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Tags", "content", c => c.String(maxLength: 100));
            AlterColumn("dbo.Accounts", "Password", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "Password", c => c.String());
            AlterColumn("dbo.Tags", "content", c => c.String());
            AlterColumn("dbo.Post", "Body", c => c.String());
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false));
        }
    }
}
