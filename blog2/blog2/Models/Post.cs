﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    [Table("Post")]
    public class Post
    {

        public int ID { set; get; }
        [Required]
        [StringLength(500,ErrorMessage="chỉ nhập tối đa 500 kí tự",MinimumLength=20)]
       
        public string Title { set; get; }
        [Required]
        [MinLength(50,ErrorMessage="tối thiểu 50 kí tự")]
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }
        public int AccountID { set; get; }

    }
}