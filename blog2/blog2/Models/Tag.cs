﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [StringLength(100,ErrorMessage="nhập tối đa 100 kí tự",MinimumLength=10)]
        public String content { set; get; }


        public virtual ICollection<Post> Posts { set; get; }

    }
}