﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [MinLength(50, ErrorMessage = "tối thiểu 50 kí tự")]
        public string Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        public String Author { set; get; }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}