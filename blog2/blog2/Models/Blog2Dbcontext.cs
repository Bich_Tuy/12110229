﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    public class Blog2Dbcontext: DbContext
    {
        public DbSet<Post> Posts { set; get; }
        public DbSet<Comment> comments { set; get; }
        public DbSet<Account> Accounts { set; get; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Post>()
                .HasMany(d => d.Tags).WithMany(p => p.Posts)// 1 post co nhieu tag-1 tag co nhieu post
                .Map(t => t.MapLeftKey("PostID").MapRightKey("TagID").ToTable("Tag_Post"));
        }

        public DbSet<Tag> Tags { get; set; }
    }
}