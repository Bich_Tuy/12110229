﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace blog2.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required]
        [DataType(DataType.Password)]
        public String Password { set; get; }
        //[Required]
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
       // [Required]
        public String Firstname { set; get; }
        public String Lastname { set; get; }


        public virtual ICollection<Post> Posts { set; get; }
        public int PostID { set; get; }
    }
}